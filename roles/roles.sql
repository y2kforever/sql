CREATE USER dbadmin INHERIT;
CREATE USER developers INHERIT;
CREATE USER viewers INHERIT;

GRANT postgres TO dbadmin;

GRANT USAGE ON SCHEMA public to developers;
GRANT USAGE ON SCHEMA public to viewers;

GRANT USAGE ON SCHEMA history to developers;
GRANT USAGE ON SCHEMA history to viewers;

GRANT INSERT ON billing_history TO developers;
GRANT INSERT ON billing_history TO viewers;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO developers;

GRANT UPDATE ON w_billing_interval TO developers;
GRANT UPDATE ON w_mail_send_message TO developers;




GRANT SELECT ON w_report TO viewers;
GRANT SELECT ON w_position TO viewers;
GRANT SELECT ON w_location TO viewers;
GRANT SELECT ON w_person_position TO viewers;
GRANT SELECT ON w_department TO viewers;
GRANT SELECT ON w_person_department TO viewers;
GRANT SELECT ON w_mail_send_message TO viewers;
GRANT SELECT ON w_schedule TO viewers;
GRANT SELECT ON w_schedule_day TO viewers;
GRANT SELECT ON w_schedule_interval TO viewers;
GRANT SELECT ON w_temp_inout_blocked TO viewers;
