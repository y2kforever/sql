CREATE OR REPLACE FUNCTION billing_status(_companyid              INTEGER, _calculate BOOLEAN,
                                          _includebillinginterval BOOLEAN,
                                          _includepriceplan       BOOLEAN, _includepreviousinterval BOOLEAN)
  RETURNS JSONB
LANGUAGE plpgsql
AS $$
DECLARE
  _tmpIncludeBillingInterval BOOLEAN;
  _billingData               JSONB;
  _getIntervals              JSONB;

BEGIN

  _tmpIncludeBillingInterval = _includebillinginterval;

  SELECT billing_data
  INTO _billingData
  FROM billing_data(_companyid, _calculate, _includebillinginterval, _includepriceplan);

  IF (_billingData ->> 'status' IS NULL OR _includePreviousInterval) AND NOT _includebillinginterval
  THEN

    _includebillinginterval = TRUE;
    SELECT billing_data
    INTO _billingData
    FROM billing_data(_companyid, _calculate, _includebillinginterval, _includepriceplan);

  END IF;

  LOOP
    SELECT billing_add_interval(_companyid, _billingData);
    SELECT billing_data
    INTO _billingData
    FROM billing_data(_companyid, _calculate, _includebillinginterval, _includepriceplan);
    -- some computations
    EXIT WHEN _billingData ->> 'status' IS NOT NULL;
    CONTINUE WHEN _billingData ->> 'status' IS NULL;
  END LOOP;

  IF _includepreviousinterval
  THEN
    SELECT row_to_json(bi)
    INTO
      _getIntervals
    FROM w_billing_interval bi
    WHERE bi.company_id = _companyid AND
          (SELECT _billingData ->> 'start_dt') :: TIMESTAMP BETWEEN bi.start_dt AND bi.end_dt;
    _billingData = _billingData + json_build_object('previousInterval', _getIntervals);
  END IF;

  IF _billingData ->> 'billingInterval' IS NOT NULL AND NOT _tmpIncludeBillingInterval
  THEN
    _billingData = _billingData - 'billingInterval';
  END IF;


  RETURN _billingData;

END
$$;
