CREATE OR REPLACE FUNCTION billing_add_interval(_companyid INTEGER, _billinginterval JSONB)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  _type                     VARCHAR DEFAULT NULL;
  _billingSubscriptionExist BOOLEAN DEFAULT FALSE;
  _billingSettings          JSONB DEFAULT NULL;
  _createdDt                TIMESTAMP DEFAULT NOW();
  _startDt                  TIMESTAMP DEFAULT NULL;
  _endDt                    TIMESTAMP DEFAULT NULL;
  _mode                     VARCHAR DEFAULT 'auto';

BEGIN

  IF EXISTS(SELECT 1
            FROM w_billing_subscription
            WHERE company_id = _companyID AND status = 'active')
  THEN
    _billingSubscriptionExist = TRUE;
  END IF;

  SELECT row_to_json(bs)
  INTO
    _billingSettings
  FROM w_billing_settings bs
  ORDER BY bs.id DESC
  LIMIT 1;

  _startDt = _billingInterval ->> 'end_dt';
  _endDt = _billingInterval ->> 'end_dt';

  CASE
    WHEN _billingInterval ->> 'type_col' = 'trial' AND NOT _billingSubscriptionExist
    THEN
      _endDt = (SELECT _billingInterval ->> 'end_dt') :: TIMESTAMP + ((SELECT _billingSettings ->> 'expired_day') + 1)
               || ' DAYS' :: INTERVAL;
      _type = 'expired';
    WHEN _billingInterval ->> 'type_col' = 'expired' AND _billingSubscriptionExist
    THEN
      _type = 'blocked';
      _endDt = '9999-12-31 23:59:59';
    WHEN _billingInterval ->> 'type_col' = 'expired' AND NOT _billingSubscriptionExist
    THEN
      _type = 'blocked';
      _endDt = '9999-12-31 23:59:59';
    WHEN _billingInterval ->> 'type_col' = 'pending' AND _billingSubscriptionExist
    THEN
      _endDt = (SELECT _billingInterval ->> 'end_dt') :: TIMESTAMP + ((SELECT _billingSettings ->> 'expired_day') + 1)
               || ' DAYS' :: INTERVAL;
      _type = 'expired';
    WHEN _billingInterval ->> 'type_col' = 'active' AND _billingSubscriptionExist
    THEN
      _endDt = (SELECT _billingInterval ->> 'end_dt') :: TIMESTAMP + ((SELECT _billingSettings ->> 'expired_day') + 1)
               || ' DAYS' :: INTERVAL;
      _type = 'pending';
    WHEN _billingInterval ->> 'type_col' = 'active' AND NOT _billingSubscriptionExist
    THEN

      _endDt = (SELECT _billingInterval ->> 'end_dt') :: TIMESTAMP + ((SELECT _billingSettings ->> 'expired_day') + 1)
               || ' DAYS' :: INTERVAL;
      _type = 'expired';

  END CASE;

  INSERT INTO w_billing_interval (company_id, user_id, priceplan_id, employee_limit, price, type_col, mode_col, payment_method_id, settings_id, manual_day, start_dt, end_dt, next_billing_dt, created_dt, updated_dt, description)
  VALUES
    (_companyID, NULL, _billingInterval ->> 'priceplan_id', _billingInterval ->> 'employee_limit',
                 _billingInterval ->> 'price', _type, _mode, NULL, _billingSettings ->> 'id', NULL, _startDt, _endDt,
     NULL,
     _createdDt, NULL, NULL);
END
$$;
