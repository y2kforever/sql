CREATE OR REPLACE FUNCTION billing_data(_companyid        INTEGER, _calculate BOOLEAN, _includebillinginterval BOOLEAN,
                             _includepriceplan BOOLEAN)
  RETURNS JSONB
LANGUAGE plpgsql
AS $$
DECLARE
  _intervalRange   INTEGER DEFAULT NULL;
  _status          VARCHAR DEFAULT NULL;
  _remainingDay    INTEGER DEFAULT NULL;
  _varProgress     INTEGER DEFAULT NULL;
  _billingInterval JSONB DEFAULT NULL;
  _pricePlan       JSONB DEFAULT NULL;

BEGIN

  IF NOT EXISTS(
      SELECT 1
      FROM w_billing_interval
      WHERE company_id = _companyID AND NOW() BETWEEN start_dt AND end_dt
  )
  THEN

    IF _includeBillingInterval
    THEN
      IF _includePricePlan
      THEN

        SELECT
          row_to_json(bi),
          row_to_json(pp)
        INTO
          _billingInterval,
          _pricePlan

        FROM w_billing_interval bi
          LEFT JOIN w_price_plan pp ON pp.id = bi.priceplan_id
        WHERE company_id = _companyID
        ORDER BY id DESC
        LIMIT 1;

      ELSE
        SELECT row_to_json(bi)
        INTO _billingInterval
        FROM w_billing_interval bi
        WHERE bi.company_id = _companyID
        ORDER BY bi.id DESC
        LIMIT 1;

      END IF;
    END IF;

  ELSE

    IF _includePricePlan
    THEN

      SELECT
        row_to_json(bi),
        row_to_json(pp)
      INTO
        _billingInterval,
        _pricePlan

      FROM w_billing_interval bi
        LEFT JOIN w_price_plan pp ON pp.id = bi.priceplan_id
      WHERE bi.company_id = _companyID AND NOW() BETWEEN bi.start_dt AND bi.end_dt AND bi.updated_dt IS NULL;

    ELSE

      SELECT row_to_json(bi)
      INTO _billingInterval
      FROM w_billing_interval bi
      WHERE bi.company_id = _companyID AND NOW() BETWEEN bi.start_dt AND bi.end_dt AND bi.updated_dt IS NULL;

    END IF;

    _status = _billingInterval ->> 'type_col';

    IF _billingInterval ->> 'type_col' <> 'blocked' AND _calculate
    THEN

      _intervalRange = (SELECT _billingInterval ->> 'end_dt') :: DATE -
                       (SELECT _billingInterval ->> 'start_dt') :: DATE;
      _remainingDay = (SELECT _billingInterval ->> 'end_dt') :: DATE - NOW() :: DATE;
      _varProgress = ROUND(_remainingDay * 100 / _intervalRange);

    END IF;

  END IF;


  RETURN json_build_object(
      'status', _status,
      'remainingDay', _remainingDay,
      'varProgress', _varProgress,
      'billingInterval', CASE
                         WHEN _includeBillingInterval
                           THEN _billingInterval
                         ELSE NULL
                         END,
      'pricePlan', CASE
                   WHEN _includePricePlan
                     THEN _pricePlan
                   ELSE NULL
                   END

  );
END
$$;
